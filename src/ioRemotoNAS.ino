//
// Firmware para Placa Azul, 16E/16S, sem dimmer, IP, MAC e LinkIO via jumper
// Arduino Nano com shield ethernet baseado no chip ENC28J60
// Parte do IP e MAC via jumpers: 192.168.0.XXX, onde XXX = 20 ~ 27 (IP0_PIN, IP1_PIN e IP2_PIN)
// ED14 e ED15 desacopladas das saídas via jumper (LINK_IO_PIN)
//
// Pinos usados pelo shield ethernet: 10, 11, 12 e 13
// Pinos usados para saídas digitais: 2, 3, 4, e 5
// Pinos usados para entradas digitais: 6, 7 e 8
// Pinos usados para IP e Mac: 14, 15 e 16
// Pino usado para LinkIO: 17
//
// Os arquivos das bibliotecas Modbus e EtherCard estão na pasta do projeto, então não é preciso instalar nada na IDE do Arduino
// A biblioteca tinha um bug, que foi corrigido por mim

#include "Modbus.h"
#include "ModbusIP_ENC28J60.h"
#include "EtherCard.h"
#include <TimerOne.h>
#include <EEPROM.h>

word Trigger_ed;
word memTrigger_ed;
word memoria_sd;

struct dt_timer
{
  unsigned int preset;
  unsigned int efetivo;
  bool enable;
  bool done;
  bool timing;
  bool reset;
};
#define QTE_TMR 8
dt_timer timer[QTE_TMR];

// ModbusIP object
ModbusIP mb;

// Endereçamento modbus
// Registers
#define ADDR_MODBUS_CONFIG_LINK_IO 0

// Coils
#define ADDR_MODBUS_COIL_SD_0 1                 // Coils 1 até 16
#define ADDR_MODBUS_COIL_ED_0 17                // Coils 17 até 32
#define ADDR_MODBUS_COIL_DIPSWITCH_1 33         // Coil 33
#define ADDR_MODBUS_COIL_DIPSWITCH_2 34         // Coil 34
#define ADDR_MODBUS_COIL_DIPSWITCH_3 35         // Coil 35
#define ADDR_MODBUS_COIL_DIPSWITCH_4 36         // Coil 36
#define ADDR_MODBUS_COIL_DIPSWITCH_5 37         // Coil 37
#define ADDR_MODBUS_COIL_DIPSWITCH_6 38         // Coil 38
#define ADDR_MODBUS_COIL_DIPSWITCH_7 39         // Coil 39
#define ADDR_MODBUS_COIL_DIPSWITCH_8 40         // Coil 40

// Pinagem
#define NANO7_OUTPUT_LD 7
#define NANO3_SCLK 3
#define NANO4_SDOUT 4
#define NANO8_OUTS_ON 8
#define NANO6_INPUT_LD 6
#define NANO5_SDIN 5
#define IP0_PIN 14
#define IP1_PIN 15
#define IP2_PIN 16
#define LINK_IO_PIN 17

//*************************  S E T U P  *******************************
void setup()
{
  pinMode(NANO8_OUTS_ON, OUTPUT);
  // desabilita saidas digitais
  digitalWrite(NANO8_OUTS_ON, HIGH);

  pinMode(IP0_PIN, INPUT_PULLUP);
  pinMode(IP1_PIN, INPUT_PULLUP);
  pinMode(IP2_PIN, INPUT_PULLUP);
  pinMode(LINK_IO_PIN, INPUT_PULLUP);
  pinMode(NANO3_SCLK, OUTPUT);
  pinMode(NANO6_INPUT_LD, OUTPUT);
  pinMode(NANO5_SDIN, INPUT);
  pinMode(NANO7_OUTPUT_LD, OUTPUT);
  pinMode(NANO3_SCLK, OUTPUT);
  pinMode(NANO4_SDOUT, OUTPUT);

  Serial.begin(9600);

  // Configura a interrupcao do Timer1 para 10ms
  Timer1.attachInterrupt(interrupcaoTimer);
  Timer1.initialize(10000); // 10ms

  timer[0].enable = 1;
  
  // Modbus - REGISTERS alocação dos registros
  mb.addHreg(0);


  // Modbus - COILS alocação das saidas digitais
  for (int i = 0; i <= 15; i++)
    mb.addCoil(ADDR_MODBUS_COIL_SD_0 + i);
    
  // Modbus - COILS alocação das entradas digitais
  for (int i = 0; i <= 15; i++)
    mb.addCoil(ADDR_MODBUS_COIL_ED_0 + i);

  // Modbus - COILS alocação dos dip switch
  mb.addCoil(ADDR_MODBUS_COIL_DIPSWITCH_1);
  mb.addCoil(ADDR_MODBUS_COIL_DIPSWITCH_2);
  mb.addCoil(ADDR_MODBUS_COIL_DIPSWITCH_3);
  mb.addCoil(ADDR_MODBUS_COIL_DIPSWITCH_4);
  mb.addCoil(ADDR_MODBUS_COIL_DIPSWITCH_5);
  mb.addCoil(ADDR_MODBUS_COIL_DIPSWITCH_6);
  mb.addCoil(ADDR_MODBUS_COIL_DIPSWITCH_7);
  mb.addCoil(ADDR_MODBUS_COIL_DIPSWITCH_8); 
    

  // Restaura estado das saídas no boot
  // mb.Hreg(ADDR_MODBUS_SD, word(EEPROM.read(0), EEPROM.read(1)));
  // memoria_sd = mb.Hreg(ADDR_MODBUS_SD);
  word sd = word(EEPROM.read(0), EEPROM.read(1));
  for (int i = 0; i <= 15; i++)
    mb.Coil(ADDR_MODBUS_COIL_SD_0 + i, bitRead(sd, i));


  // Atualiza entradas digitais
  entradasDigitais();

  
  // Obtendo IP dos jumpers
  byte address = 20;
  if (mb.Coil(ADDR_MODBUS_COIL_DIPSWITCH_1))
    address += 1;
  if (mb.Coil(ADDR_MODBUS_COIL_DIPSWITCH_2))
    address += 2;
  if (mb.Coil(ADDR_MODBUS_COIL_DIPSWITCH_3))
    address += 4;
  if (mb.Coil(ADDR_MODBUS_COIL_DIPSWITCH_4))
    address += 8;


  Serial.print("IP: 192.168.0.");
  Serial.println(address);

  byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, address};
  byte ip[] = {192, 168, 0, address};
  mb.config(mac, ip);


  // Obtendo LINK IO dos jumpers
  if (mb.Coil(ADDR_MODBUS_COIL_DIPSWITCH_5))
    mb.Hreg(ADDR_MODBUS_CONFIG_LINK_IO, 0x3FFF);
  else
    mb.Hreg(ADDR_MODBUS_CONFIG_LINK_IO, 0xFFFF);

  Serial.print("ConfigLinkIO: 0x");
  Serial.println(mb.Hreg(ADDR_MODBUS_CONFIG_LINK_IO), HEX);

  // habilita saidas digitais
  digitalWrite(NANO8_OUTS_ON, LOW);
}

//************************  LOOP  **************************************
void loop()
{
  // Call once inside loop() - all modbus magic here
  mb.task();

  // Atualiza entradas digitais
  entradasDigitais();

  // Rotina dos temporizadores auxiliares
  timer10ms();

  // Rotina do trigger
  trigger();

  // Ativacao das saidas digitais
  ativacao();

  // Temporizador 120s
  timer[0].preset = 12000;
  timer[0].reset = 0;
  timer[0].enable = !timer[0].done;
  if (timer[0].done)
  {
    // Serial.println("timer 2s");

    word sd = 0;
    for (int i = 0; i <= 15; i++)
    {
      bitWrite(sd, i, mb.Coil(ADDR_MODBUS_COIL_SD_0 + i));
    }
    // if (mb.Hreg(ADDR_MODBUS_SD) != memoria_sd)
    if (sd != memoria_sd)
    {
      // EEPROM.update(0, highByte(mb.Hreg(ADDR_MODBUS_SD)));
      // EEPROM.update(1, lowByte(mb.Hreg(ADDR_MODBUS_SD)));
      // memoria_sd = mb.Hreg(ADDR_MODBUS_SD);
      EEPROM.update(0, highByte(sd));
      EEPROM.update(1, lowByte(sd));
      memoria_sd = sd;
    }
  }
}

/////////////////////////////////////////////////// timer10ms()
void timer10ms()
{
  for (int i = QTE_TMR - 1; i >= 0; i--)
  {
    // Efetivo >= Preset
    if (timer[i].efetivo >= timer[i].preset)
    {
      timer[i].done = 1;
      timer[i].timing = 0;
    }
    else
    // Efetivo < Preset
    {
      timer[i].done = 0;
      timer[i].timing = 1;
    }

    // Reset ou nao habilitado
    if (timer[i].reset || !timer[i].enable)
    {
      timer[i].efetivo = 0;
      timer[i].done = 0;
      timer[i].timing = 0;
    }
  }
}

/////////////////////////////////////////////////// interrupcaoTimer()
void interrupcaoTimer()
{
  // Incrementa efetivo dos temporizadores auxiliares
  for (int i = QTE_TMR - 1; i >= 0; i--)
    if ((timer[i].enable) && timer[i].timing)
      timer[i].efetivo += 1;
}

/////////////////////////////////////////////////// trigger()
void trigger()
{
  for (int i = 15; i >= 0; i--)
  {
    // Trigger ed
    if (mb.Coil(ADDR_MODBUS_COIL_ED_0 + i) && !bitRead(memTrigger_ed, i))
      bitWrite(Trigger_ed, i, true);
    else
      bitWrite(Trigger_ed, i, false);

    bitWrite(memTrigger_ed, i, mb.Coil(ADDR_MODBUS_COIL_ED_0 + i));
  }
}

/////////////////////////////////////////////////// ativacao()
void ativacao()
{
  for (int i = 15; i >= 0; i--)
    // Da um toogle na saida de acordo com ed (verifica cfgLinkIO)
    if (bitRead(Trigger_ed, i) && bitRead(mb.Hreg(ADDR_MODBUS_CONFIG_LINK_IO), i))
      mb.Coil(ADDR_MODBUS_COIL_SD_0 + i, !mb.Coil(ADDR_MODBUS_COIL_SD_0 + i));
  saidasDigitais();
}

/////////////////////////////////////////////////// saidasDigitais()
void saidasDigitais()
{
  // Desliga Clock e Enable
  digitalWrite(NANO7_OUTPUT_LD, LOW);
  digitalWrite(NANO3_SCLK, LOW);

  for (int i = 15; i >= 0; i--)
  {
    // Atualiza Out
    digitalWrite(NANO4_SDOUT, !mb.Coil(ADDR_MODBUS_COIL_SD_0 + i));

    // Liga Clock
    digitalWrite(NANO3_SCLK, HIGH);

    // Desliga Clock
    digitalWrite(NANO3_SCLK, LOW);
  }

  // Liga Enable
  digitalWrite(NANO7_OUTPUT_LD, HIGH);
}

/////////////////////////////////////////////////// entradasDigitais()
void entradasDigitais()
{
  static word entradas;
  static int filtro;
  static word ed_SemFiltroMemorizada;
  word ed_SemFiltro = 0;
  unsigned long ed_raw = 0;

  // Liga o enable
  digitalWrite(NANO6_INPUT_LD, HIGH);

  for (int i = 23; i >= 0; i--)
  {
    // Atualiza entrada
    if (!digitalRead(NANO5_SDIN))
      bitWrite(ed_raw, i, !digitalRead(NANO5_SDIN));

    // Desliga o clock
    digitalWrite(NANO3_SCLK, LOW);

    // Liga o clock
    digitalWrite(NANO3_SCLK, HIGH);
  }

  //// Desliga o enable
  digitalWrite(NANO6_INPUT_LD, LOW);

  //Serial.print("ed_raw: 0x");
  //Serial.println(ed_raw, HEX);

  // le dip switch
      mb.Coil(ADDR_MODBUS_COIL_DIPSWITCH_1, bitRead(ed_raw, 0));
      mb.Coil(ADDR_MODBUS_COIL_DIPSWITCH_2, bitRead(ed_raw, 1));
      mb.Coil(ADDR_MODBUS_COIL_DIPSWITCH_3, bitRead(ed_raw, 2));
      mb.Coil(ADDR_MODBUS_COIL_DIPSWITCH_4, bitRead(ed_raw, 3));
      mb.Coil(ADDR_MODBUS_COIL_DIPSWITCH_5, bitRead(ed_raw, 4));
      mb.Coil(ADDR_MODBUS_COIL_DIPSWITCH_6, bitRead(ed_raw, 5));
      mb.Coil(ADDR_MODBUS_COIL_DIPSWITCH_7, bitRead(ed_raw, 6));
      mb.Coil(ADDR_MODBUS_COIL_DIPSWITCH_8, bitRead(ed_raw, 7));
  // Serial.println(mb.Coil(ADDR_MODBUS_COIL_DIPSWITCH_1)); 

  //// Inverte bytes
  ed_raw = ed_raw >> 8;
  ed_SemFiltro = (lowByte(ed_raw) << 8) + highByte(ed_raw);

  // Filtro
  if (ed_SemFiltro == ed_SemFiltroMemorizada)
    filtro -= 1;
  else
    filtro = 50;
  ed_SemFiltroMemorizada = ed_SemFiltro;

  if (filtro == 0)
  {
    entradas = ed_SemFiltro;
    for (int i = 0; i <= 15; i++)
      mb.Coil(ADDR_MODBUS_COIL_ED_0 + i, bitRead(entradas, i));
    // return entradas;
  }
}   
