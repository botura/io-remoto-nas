# Firmware do IO Remoto usado no NAS

Placa usada na automação residencial, usando Arduino Nano como controlador, entradas e saídas com isolação óptica.

Geralmente as entradas são conectadas num interruptor tipo campainha (push button) e as saídas a um relê de interface para acionamento da ilumninação.

Cada vez que a entrada é pulsada o estado da respectiva saída é invertido. Então cada entrada é diretamente relacionada com o estado de uma saída, a esse acoplamento dei o nome de LinkIO. A entrada 1 é acoplada à saída 1, a entrada 2 à saída 2 e assim sucessivamente.


## Características
  * Alimentação 12Vcc
  * Alimentação do Arquino 5Vcc
  * Entradas e saídas tipo P
  * Comunicação Modbus TCP
  * 16 entradas digitais
  * 16 saídas digitais
  * 8 dip switch de configuração da placa

## Dip switch
  * 1-4: Endereço IP: 192.168.0.20 a 192.168.0.35
  * 5: LinkIO: On: E15 e E16 desacopladas da S15 e S16
  * 6: Reserva
  * 7: Reserva
  * 8: Reserva

## Endereçamento modbus
  * Coil 1 a 16: Saídas (R/W)
  * Coil 17 a 32: Entradas (R)
  * Coil 33 a 40: Estado do dip switch
  * Register 1: LinkIO